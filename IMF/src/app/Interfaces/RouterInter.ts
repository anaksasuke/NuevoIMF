export interface RouterInter {
    nombre:string;
    children?:RouterInter[];
    router?:string;
}