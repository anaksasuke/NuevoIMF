export class ProveedorTel {
    constructor(
        public idProveedor:number, 
        public codigo:number,
        public telefono:number
    ){}
}