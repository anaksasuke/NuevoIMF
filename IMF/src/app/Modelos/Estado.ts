export class  Estado {
    constructor(
        public idPais:number,
        public idEstado:number,
        public nomEstado:string
    ) {}
}