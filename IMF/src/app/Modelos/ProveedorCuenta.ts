export class ProveedorCuenta {
    constructor(
        public idProveedor:number,
        public idBanco:number,
        public cuenta:number
    ){}
}