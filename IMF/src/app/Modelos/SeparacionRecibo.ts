export class SeparacionRecibo {
    constructor(
        public idPago:number,
        public idParcela:number,
        public idLote:number,
        public idCliente:number,
        public ubicacion:number
    ){}
}