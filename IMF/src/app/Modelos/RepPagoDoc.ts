export class ReportPagosDocument {
    constructor(
        public idDocumento:number,
        public idCliente:number,
        public idParcela:number,
        public idLote:number
    ){}
}