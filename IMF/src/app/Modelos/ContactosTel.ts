export class ContactosTel {
    constructor(
        public idCliente:number,
        public idContacto:number,
        public codigo:number,
        public numero:number
    ){}
}