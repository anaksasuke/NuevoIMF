export class Ciudad {
    constructor(
        public idPais:number,
        public idEstado:number,
        public idCiudad:number,
        public nomCiudad:string
    ){}
}