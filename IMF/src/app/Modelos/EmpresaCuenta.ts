export class EmpresaCuenta {
    constructor(
        public idEmpresa:number,
        public idBanco:number,
        public cuenta:number,
    ){}
} 