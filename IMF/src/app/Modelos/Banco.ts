export class Banco {
    constructor(
        public idBanco:number,
        public nombre:string,
        public RFC:string,
        public razon:string
    ){}
}