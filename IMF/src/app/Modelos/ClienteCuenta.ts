export class ClienteCuenta {
    constructor(
        public idCliente:number, 
        public idBanco:number,
        public cuenta:number
    ){}
}