export class PagoVenta {
    constructor(
        public idPago:number,
        public idParcela:number,
        public idLote:number,
        public idCliente:number,
        public nombreCliente:string,
        public idDocumento:number,
        public idUsuario:number,
        public idForma:number,
        public idTipo:number,
        public observacion:string,
        public importe:number,
        public restante:number,
        public reporte:string
    ){}
}